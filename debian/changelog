mit-scheme (11.2-2) unstable; urgency=medium

  * remove i386 from architecture list (closes: #982791)
  * add postrm script (closes: #987373)

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 25 Aug 2021 11:40:16 +0100

mit-scheme (11.2-1) unstable; urgency=medium

  * new upstream release
  * check signature in debian/watch
  * remove upstream maintenance quilt patches

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 11 Mar 2021 00:54:23 +0000

mit-scheme (11.1-5) unstable; urgency=medium

  * track upstream maintenance branch as quilt patches
  * hope it compiles on i386

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 18 Feb 2021 14:09:59 +0000

mit-scheme (11.1-4) unstable; urgency=medium

  * unified -arch and -indep!
  * name executable and man page symmetrically: with arch
  * fiddle with update-alternatives so
    - mit-scheme can be some arch-specific mit-scheme
    - scheme can be mit-scheme

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 10 Feb 2021 11:48:19 +0000

mit-scheme (11.1-3) unstable; urgency=medium

  * quilt patch to increase verbosity of texi2dvi
  * fracture build dependencies by -arch and -indep
  * dependency on texlive-plain-generic for epsf.tex

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 09 Feb 2021 17:08:35 +0000

mit-scheme (11.1-2) unstable; urgency=medium

  * build dependency on ghostscript for eps2eps

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 09 Feb 2021 11:59:25 +0000

mit-scheme (11.1-1) unstable; urgency=medium

  * new upstream release
  * bump policy
  * forward port quilt patches
  * --enable-cross-compiling when using old version to build new version
  * quilt patch to avoid missing extern compilation warning

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 09 Feb 2021 11:02:06 +0000

mit-scheme (10.1.11-2) unstable; urgency=medium

  * Use dh_link support file *.links instead of override_:
  * mit-scheme(1) belongs in the binary package, but is auto-installed
    when the documentation package is built. Instead of laboriously
    working around this just install manually (closes: #962575)

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 10 Jun 2020 14:21:47 +0100

mit-scheme (10.1.11-1) unstable; urgency=medium

  * new upstream release
  * bump debhelper from old 12 to 13
  * set upstream metadata fields: Repository, Repository-Browse
  * update standards version to 4.5.0, no changes needed
  * forward port quilt patches (context includes version)
  * merge debian mit-scheme(1) into quilt patch to upstream mit-scheme(1)
  * info dir file should not be installed

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 09 Jun 2020 12:09:43 +0100

mit-scheme (10.1.10-1) unstable; urgency=medium

  * new upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 12 Aug 2019 11:31:26 +0200

mit-scheme (10.1.9-2) unstable; urgency=medium

  * bump policy

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 08 Aug 2019 11:06:09 +0200

mit-scheme (10.1.9-1) unstable; urgency=medium

  * new upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 13 Jun 2019 17:54:34 +0100

mit-scheme (10.1.7-1) unstable; urgency=medium

  * new upstream version

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 05 Jun 2019 20:27:44 +0100

mit-scheme (10.1.6-1) unstable; urgency=medium

  * new upstream release
  * specify dh12 the new way

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 12 Apr 2019 01:03:16 +0100

mit-scheme (10.1.5-1) unstable; urgency=medium

  * new upstream release

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 01 Feb 2019 12:05:39 +0000

mit-scheme (10.1.4-1) unstable; urgency=medium

  * new upstream release

 -- Barak A. Pearlmutter <bap@debian.org>  Mon, 14 Jan 2019 12:20:52 +0000

mit-scheme (10.1.3-2) unstable; urgency=medium

  * work around GNU tar cowardice (closes: #917808)
  * bump standards version
  * dh12
  * split debian/rules stanzas into -arch vs -indep where appropriate

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 03 Jan 2019 10:52:10 +0000

mit-scheme (10.1.3-1) unstable; urgency=medium

  * new upstream release

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 27 Nov 2018 20:03:09 +0000

mit-scheme (10.1.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Fix wrong Vcs-*

  [ Barak A. Pearlmutter ]
  * check upstream tarball signatures
  * new upstream release
  * bump standards version
  * merge / update quilt patches
  * typo in debian/rules (closes: #834651)
  * remove Chris Hanson, who has retired, as maintainer (closes: #829686)
  * install entire /usr/lib/ARCH/mit-scheme hierarchy
  * info file installation tweaks, for compression
  * manually shift .bci files from mit-scheme to mit-scheme-dbg
  * build dependency on mit-scheme (>= 10)
  * PDF file is gone: imail documentation seems info-only now
  * per policy, menu file begone
  * upstream build rife with race conditions: dh --no-parallel
  * stop removing runtime.com, needed for self-hosted rebuild
  * patch: AUXDIR_NAME needs to be fixed twice (upstream SPOT violation)
  * do not install *.la library support file
  * secure a couple URLs
  * add desktop icon and keywords
  * harden
  * lintian tag desktop-mime-but-no-exec-code
  * lintian tag public-upstream-key-not-minimal
  * secure debian/watch URL

 -- Barak A. Pearlmutter <bap@debian.org>  Sun, 11 Nov 2018 14:13:23 +0000

mit-scheme (9.1.1-5) unstable; urgency=low

  * ${DPKG_MAINTSCRIPT_ARCH} i486 is i386

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 30 Aug 2013 16:31:28 +0100

mit-scheme (9.1.1-4) unstable; urgency=low

  * use ${DPKG_MAINTSCRIPT_ARCH} in installation scripts as
    dpkg-architecture might be unavailable (closes: #721230)

 -- Barak A. Pearlmutter <bap@debian.org>  Fri, 30 Aug 2013 13:58:16 +0100

mit-scheme (9.1.1-3) unstable; urgency=low

  * patch for bracing makeinfo 5 issues

 -- Barak A. Pearlmutter <bap@debian.org>  Thu, 29 Aug 2013 23:08:30 +0100

mit-scheme (9.1.1-2) unstable; urgency=low

  * build dependency on texinfo

 -- Barak A. Pearlmutter <bap@debian.org>  Wed, 28 Aug 2013 14:47:17 +0100

mit-scheme (9.1.1-1) unstable; urgency=low

  * New upstream
  * update packaging scripts: dh9, bump policy version, etc
  * support amd64 (closes: #714887)
  * support multiarch, so i386 and amd64 can co-exist
  * allow parallel build
  * engage autoreconf
  * reduce lintian clutter from debian/control and debian/copyright

 -- Barak A. Pearlmutter <bap@debian.org>  Tue, 13 Aug 2013 09:32:08 +0200

mit-scheme (9.1-1) unstable; urgency=low

  * New upstream.
  * debian/control, debian/copyright, debian/mit-scheme-doc.*,
    debian/mit-scheme.install, debian/rules, Upstream has removed cover
    texts from documentation licenses, so merge packages mit-scheme and
    mit-scheme-doc back together.
  * debian/compat: Bump to current version.
  * debian/control: Bump standards-version to current and make
    necessary changes.
  * debian/rules: Fix lintian warnings.

 -- Chris Hanson <cph@debian.org>  Sat, 15 Oct 2011 03:08:33 -0700

mit-scheme (9.0.1-1+b1) sid; urgency=low

  * Binary-only non-maintainer upload for i386; no source changes.
  * Rebuild against libssl1.0.0

 -- i386 Build Daemon (murphy) <buildd_i386-murphy@buildd.debian.org>  Wed, 13 Apr 2011 00:54:48 +0000

mit-scheme (9.0.1-1) unstable; urgency=low

  * New upstream.
  * debian/watch: Fix, previous version was broken.

 -- Chris Hanson <cph@debian.org>  Wed, 10 Mar 2010 02:00:45 -0800

mit-scheme (9.0-1) unstable; urgency=low

  * New upstream.
  * debian/emacs*: removed.
  * debian/mit-scheme-native.1: renamed to debian/mit-scheme-i386.1.
  * debian/mit-scheme.{desktop,dirs,install,manpages,menu,postinst,prerm},
    debian/mit-scheme-i386.1, debian/control, debian/rules: updated for
    upstream changes.
  * debian/control: Bump standards-version to 3.8.4 (no changes).
  * debian/watch: Uncomment pattern for stable releases.

 -- Chris Hanson <cph@debian.org>  Sat, 27 Feb 2010 23:50:14 -0800

mit-scheme (7.7.90+20090107-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add missing build-dep on libltdl-dev (Closes: #523912)

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 26 Nov 2009 10:08:46 +0100

mit-scheme (7.7.90+20090107-1) unstable; urgency=low

  * New upstream.
  * debian/control: Bump standards-version to 3.8.0 (no changes).
  * debian/control: Add homepage field.
  * debian/copyright: Update copyright dates.

 -- Chris Hanson <cph@debian.org>  Wed, 07 Jan 2009 23:12:52 -0800

mit-scheme (7.7.90+20080130-1) unstable; urgency=low

  * New upstream.  (closes: Bug#458700, Bug#375858)
  * debian/control: Bump standards-version to 3.7.3 (no changes).
  * debian/copyright: Update copyright dates.
  * debian/copyright: Add build-dep on libdb-dev.
  * debian/mit-scheme.desktop: Add --heap arg to Exec field.
  * debian/mit-scheme-native.1: Fix lintian error:
    manpage-has-bad-whatis-entry

 -- Chris Hanson <cph@debian.org>  Wed, 30 Jan 2008 21:22:48 -0500

mit-scheme (7.7.90+20070909-1) unstable; urgency=low

  * New upstream.

 -- Chris Hanson <cph@debian.org>  Fri, 28 Sep 2007 22:11:46 -0400

mit-scheme (7.7.90+20070820-1) unstable; urgency=low

  * New upstream.
  * Fix lintian warnings:
      W: mit-scheme: desktop-entry-invalid-category
      W: mit-scheme: menu-item-uses-apps-section

 -- Chris Hanson <cph@debian.org>  Tue, 21 Aug 2007 14:16:58 -0400

mit-scheme (7.7.90+20070623-1) unstable; urgency=low

  * New upstream.
  * debian/rules, debian/mit-scheme.install,
    debian/mit-scheme-dbg.install: adjust for changes to upstream
    directory organization.
  * debian/mit-scheme.prerm, debian/mit-scheme.manpages,
    debian/mit-scheme-native.1, debian/mit-scheme.menu,
    debian/mit-scheme.desktop, debian/mit-scheme.postinst: name of
    executable has changed to "mit-scheme-native", and "mit-scheme" is now
    managed as an alternative.  This is to simplify coexistence with the
    mit-scheme-c package.
  * debian/bchscheme.1: removed since bchscheme executable no longer in
    upstream.

 -- Chris Hanson <cph@debian.org>  Sun, 24 Jun 2007 00:10:57 -0400

mit-scheme (7.7.90+20070205-1) unstable; urgency=low

  * New upstream.

 -- Chris Hanson <cph@debian.org>  Mon, 5 Feb 2007 13:04:49 -0500

mit-scheme (7.7.90+20060906-3) unstable; urgency=low

  * No changes; rebuild with downgraded openssl in order to permit
    transition into testing.

 -- Chris Hanson <cph@debian.org>  Wed, 20 Sep 2006 21:59:42 -0400

mit-scheme (7.7.90+20060906-2) unstable; urgency=low

  * debian/mit-scheme.emacsen-remove: don't purge if directory doesn't
    exist.  (closes: Bug#387788)

 -- Chris Hanson <cph@debian.org>  Tue, 19 Sep 2006 12:43:46 -0400

mit-scheme (7.7.90+20060906-1) unstable; urgency=low

  * New upstream.
  * Fix Priority of mit-scheme-dbg to match overrides.
  * Use debhelper v5.
  * Add icon to menu entry.
  * Add debian/watch file.
  * Bump standards-version (no changes).

 -- Chris Hanson <cph@debian.org>  Sat,  9 Sep 2006 20:54:21 -0400

mit-scheme (7.7.90+20060414-1) unstable; urgency=low

  * New upstream snapshot.
  * Move documentation to new package with separate "upstream".
  * Move .bci files into separate package.  (closes: Bug#358339)

 -- Chris Hanson <cph@debian.org>  Sat, 15 Apr 2006 21:18:11 -0400

mit-scheme (7.7.90+20060201-1) unstable; urgency=low

  * New upstream snapshot.

 -- Chris Hanson <cph@debian.org>  Wed, 1 Feb 2006 22:49:37 -0500

mit-scheme (7.7.90+20050912-2) unstable; urgency=low

  * Fix broken doc-base files.  (closes: Bug#345096)
  * Fix Emacs control scripts to match current policy.
    (closes: Bug#328897)
  * Change "etc/xscheme.el" to refer to "MIT/GNU Scheme".
  * Patch "src/microcode/prdb4.c" to work with DB 4.3 and 4.4.
  * Update FSF address "debian/copyright".
  * Add documentation license to "debian/copyright".

 -- Chris Hanson <cph@debian.org>  Fri, 13 Jan 2006 12:45:43 -0500

mit-scheme (7.7.90+20050912-1) unstable; urgency=low

  * Acknowledge NMU (thanks Matej!).  (closes: Bug#323739)
  * New upstream snapshot.
  * Bump standards version to 3.6.2 (no changes).
  * Drop texi2html from build dependencies; no longer used.

 -- Chris Hanson <cph@debian.org>  Mon, 12 Sep 2005 21:36:33 -0400

mit-scheme (7.7.90-5.1) unstable; urgency=low

  * NMU during BSP.
  * Fix build failure with gcc 4.0.  Closes: #323739.
    - src/microcode/obstack.h: Casts can no longer be combined with `++'
      (backported from the current glibc version).
    - src/microcode/uxio.c: Include <string.h> for strlen.
  * doc/*/Makefile.in: texi2html now creates a subdirectory by default;
    invoke it with `--output=.'.

 -- Matej Vela <vela@debian.org>  Mon, 12 Sep 2005 10:13:58 +0200

mit-scheme (7.7.90-5) unstable; urgency=low

  * Add "libx11-dev" to build-depends.  (closes: Bug#290845)
  * Fix debian/control and debian/menu to eliminate some lintian errors
    and warnings.

 -- Chris Hanson <cph@debian.org>  Tue, 18 Jan 2005 00:33:57 -0500

mit-scheme (7.7.90-4) unstable; urgency=low

  * Fix problem with CPUID detection on older 486 processors.

 -- Chris Hanson <cph@debian.org>  Fri, 31 Oct 2003 21:10:55 -0500

mit-scheme (7.7.90-3) unstable; urgency=low

  * Add --no-site-file to emacsen-install.  Update syntax and formatting
    of emacs control files.  (closes: Bug#217903)

 -- Chris Hanson <cph@debian.org>  Mon, 27 Oct 2003 23:45:46 -0500

mit-scheme (7.7.90-2) unstable; urgency=low

  * Fix fatal bug in GC indicator when run under Emacs.
    (closes: Bug#212958)

 -- Chris Hanson <cph@debian.org>  Wed, 15 Oct 2003 13:17:19 -0400

mit-scheme (7.7.90-1) unstable; urgency=low

  * New upstream unstable release.
  * Use dynamic modules again.
  * Standards version 3.6.1 (no changes).

 -- Chris Hanson <cph@debian.org>  Thu, 18 Sep 2003 23:44:41 -0400

mit-scheme (7.7.1-2) unstable; urgency=low

  * Change program name to MIT/GNU Scheme; this is now a GNU program.
  * Don't exclude PDF files from compression; zxpdf will handle them.
  * Link crypto modules normally, now that crypto is allowed in main.
  * Use dh_installman to install man pages.
  * Update debian/rules according to latest advice from autotools-dev.
  * Use debian/compat instead of DH_COMPAT; use debhelper v4.
  * Bump standards version to 3.5.10: new DEB_BUILD_OPTIONS handling.

 -- Chris Hanson <cph@debian.org>  Tue, 3 Jun 2003 17:57:24 -0400

mit-scheme (7.7.1-1) unstable; urgency=low

  * New upstream version.

 -- Chris Hanson <cph@debian.org>  Tue, 18 Jun 2002 01:37:28 -0400

mit-scheme (7.7.0-1) unstable; urgency=low

  * New upstream version.

 -- Chris Hanson <cph@debian.org>  Thu, 14 Mar 2002 17:04:07 -0500

mit-scheme (7.6.1-2) unstable; urgency=low

  * Make sure scheme-program-name is defined for XEmacs.
    (closes: Bug#135556)

 -- Chris Hanson <cph@debian.org>  Mon, 25 Feb 2002 14:54:55 -0500

mit-scheme (7.6.1-1) unstable; urgency=low

  * New upstream version fixes fatal bug on certain AMD Athlon processors.
  * Add tetex-extra to build-depends, since texi2pdf seems to need it.

 -- Chris Hanson <cph@debian.org>  Mon, 28 Jan 2002 23:26:13 -0500

mit-scheme (7.6.0-3) unstable; urgency=low

  * Another update to "xscheme.el"; this works around a race condition in
    interrupt signalling.

 -- Chris Hanson <cph@debian.org>  Mon, 3 Dec 2001 13:53:56 -0500

mit-scheme (7.6.0-2) unstable; urgency=low

  * Use updated version of "xscheme.el".  (closes: Bug#122105)
  * Add changes recommended by autotools-dev package.

 -- Chris Hanson <cph@debian.org>  Mon, 3 Dec 2001 01:08:10 -0500

mit-scheme (7.6.0-1) unstable; urgency=low

  * New upstream version.

 -- Chris Hanson <cph@debian.org>  Mon, 26 Nov 2001 15:43:15 -0500

mit-scheme (7.5.17-3) unstable; urgency=low

  * Add manpages.
  * Eliminate lintian error for undeclared conffile.

 -- Chris Hanson <cph@debian.org>  Thu, 4 Oct 2001 17:06:28 -0400

mit-scheme (7.5.17-2) unstable; urgency=low

  * Add code to manage the "scheme" executable using update-alternatives.
    (closes: Bug#112948)
  * Add "Description-fr" to "debian/control".
  * Update standards-version to 3.5.6.

 -- Chris Hanson <cph@debian.org>  Thu, 20 Sep 2001 15:22:55 -0400

mit-scheme (7.5.17-1) unstable; urgency=low

  * New upstream version.

 -- Chris Hanson <cph@debian.org>  Wed, 18 Jul 2001 23:19:20 -0400

mit-scheme (7.5.16-3) unstable; urgency=low

  * Fix problem with code that builds bands.  Previous revision was
    completely broken due to this bug.

 -- Chris Hanson <cph@debian.org>  Sun, 29 Apr 2001 22:48:52 -0400

mit-scheme (7.5.16-2) unstable; urgency=low

  * Acknowledge bug report for missing man page.  (closes: Bug#95377)
  * Update to standards-version 3.5.3.

 -- Chris Hanson <cph@debian.org>  Sun, 29 Apr 2001 20:49:04 -0400

mit-scheme (7.5.16-1) unstable; urgency=low

  * New upstream release.

 -- Chris Hanson <cph@debian.org>  Mon, 19 Mar 2001 23:35:23 -0500

mit-scheme (7.5.15-1) unstable; urgency=low

  * New upstream release provides support for dynamic loading of crypto
    packages.  This allows the crypto support to be present without
    forcing a dependency that would preclude putting mit-scheme in the
    "main" section.

 -- Chris Hanson <cph@debian.org>  Thu, 8 Mar 2001 22:25:27 -0500

mit-scheme (7.5.14-2) unstable; urgency=low

  * Don't link with any crypto code, as required by policy for packages in
    "main".  (closes: Bug#88650)

 -- Chris Hanson <cph@debian.org>  Tue, 6 Mar 2001 00:23:58 -0500

mit-scheme (7.5.14-1) unstable; urgency=low

  * New upstream release allows dynamic linking to crypto libraries, and
    supports OpenSSL.  Previous release was statically linked against
    unpackaged Blowfish library.
  * Updated to standards-version 3.5.2.

 -- Chris Hanson <cph@debian.org>  Mon, 5 Mar 2001 11:46:44 -0500

mit-scheme (7.5.13-2) unstable; urgency=low

  * Change broken relative symbolic link to "/etc/mime.types" to an
    absolute link.
  * Add missing build-depends and depends.

 -- Chris Hanson <cph@debian.org>  Sun, 25 Feb 2001 16:40:45 -0500

mit-scheme (7.5.13-1) unstable; urgency=low

  * Initial Debian release.  (closes: Bug#82641)

 -- Chris Hanson <cph@debian.org>  Sat, 24 Feb 2001 01:17:12 -0500
